<?php

namespace Duna\Plugin\Localization\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="plugin_localization")
 * @ORM\Entity
 *
 * @method setCode(string $code)
 * @method string getCode()
 * @method setName(string $name)
 * @method string getName()
 * @method boolean getDefault()
 */
class Localization
{

    use \Kdyby\Doctrine\Entities\MagicAccessors;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=20, nullable=false)
     */
    protected $name;
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=6, nullable=false)
     */
    protected $code;
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_default", type="boolean", nullable=false, unique=false)
     */
    protected $default = false;
    /**
     * @var integer
     *
     * @ORM\Column(name="localization_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setDefault()
    {
        $this->default = true;
        return $this;
    }

}
