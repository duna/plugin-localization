<?php

namespace Duna\Localization\Fixtures\Init;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Duna\Plugin\Localization\Facade;

class InitFixture extends AbstractFixture
{

    private $defaults = [
        'cs' => 'Čeština',
        'en' => 'English',
    ];

    public function load(ObjectManager $manager)
    {
        $facade = new Facade($manager);
        $i = 0;
        foreach ($this->defaults as $key => $value) {
            $temp = $facade->getOneByCode($key);
            if ($temp === null)
                $facade->insert($key, $value, ++$i == 1, false);
            else
                $facade->update($temp->id, $key, $value, ++$i == 1);
        }
        $manager->flush();
    }
}