<?php

namespace Duna\Localization\Localization;

use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 */
class LocaleFilter extends SQLFilter
{

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if (!$targetEntity->reflClass->implementsInterface(ILocale::class))
            return "";

        return $targetTableAlias . '.plugin_localization_id = ' . $this->getParameter('locale');
    }

}
