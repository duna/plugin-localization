<?php

namespace Duna\Plugin\Localization\DI;

use Duna\Console\IEntityConsoleProvider;
use Duna\DI\Extensions\CompilerExtension;
use Duna\Plugin\Localization\Cli\InstallCommand;
use Duna\Plugin\Manager\Entity\Plugin;
use Duna\Plugin\Manager\IPlugin;
use Kdyby\Doctrine\DI\IEntityProvider;

class Extension extends CompilerExtension implements IPlugin, IEntityProvider, IEntityConsoleProvider
{
    private $commands = [
        InstallCommand::class,
    ];

    /**
     * @inheritDoc
     */
    function getEntityMappings()
    {
        return self::entityMappings();
    }

    public function loadConfiguration()
    {
        $this->addCommands($this->commands);
    }

    public static function entityMappings()
    {
        return ['Duna\\Plugin\\Localization\\Entity' => __DIR__ . '/../Entity'];
    }

    /**
     * @inheritDoc
     */
    public static function getPluginInfo()
    {
        $entity = new Plugin();
        $entity->name = 'Localization';
        $entity->description = 'Plugin localization';

        return [
            'plugin' => $entity,
        ];
    }

}