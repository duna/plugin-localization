<?php

namespace Duna\Plugin\Localization;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Duna\Plugin\Localization\Entity\Localization;
use Nette\InvalidStateException;

class Facade
{
    /** @var  \Doctrine\ORM\EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param $id
     * @param bool $throwException
     * @return null|\Duna\Plugin\Localization\Entity\Localization
     */
    public function getById($id, $throwException = false)
    {
        $entity = $this->em->getRepository(Localization::class)->find($id);

        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    public function getOneByCode($code, $throwException = false)
    {
        $entity = $this->em->getRepository(Localization::class)->findOneBy([
            'code' => $code,
        ]);
        if ($entity === null && $throwException)
            throw new InvalidStateException();

        return $entity;
    }

    public function insert($code, $name, $default = false, $throwException = false)
    {
        $entity = new Localization();
        $entity->name = $name;
        $entity->code = $code;
        $entity->setDefault($default);

        $this->em->persist($entity);
        try {
            $this->em->flush($entity);
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw new InvalidStateException();
        }
    }

    public function update($id, $code, $name, $default, $throwException = false)
    {
        try {
            $entity = $this->getById($id, true);
            $entity->code = $code;
            $entity->name = $name;
            $entity->setDefault($default);
            $this->em->persist($entity);
            $this->em->flush($entity);
        } catch (UniqueConstraintViolationException $e) {
            if ($throwException)
                throw new InvalidStateException();
        } catch (InvalidStateException $e) {
            if ($throwException)
                throw new InvalidStateException();
        }
    }
}